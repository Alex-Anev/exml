defmodule Exml do
  # does not handle unformatted XML
  # does not handle attributes of XML tags
  def to_json(from, to) do
    {:ok, xml} = File.read(from)

    json_scope =
      String.split(xml, "\n", trim: true)
      |> Enum.map(&String.trim/1)
      |> xml_to_json([])
      |> List.to_string()
      # removes comma from last line
      |> String.replace(",}", "}")
      # adds comma after closing tag
      |> String.replace("}\"", "},\"")

    json = "{" <> json_scope <> "}"
    File.open(to, [:read, :write])
    File.write(to, json)
    # to # uncomment to output directory to created json file
  end

  defp xml_to_json([], result), do: Enum.reverse(result)

  defp xml_to_json([hd | tl], result) do
    cond do
      # > ... < # KEY & VALUE
      String.match?(hd, ~r/>([^;]*)</) ->
        # lazy hack but it works
        for_regex = String.replace_trailing(hd, ">", "")

        key =
          Regex.run(~r/<([^;]*)>/, for_regex)
          |> tl()
          |> List.to_string()

        value =
          Regex.run(~r/>([^;]*)</, hd)
          |> tl()
          |> List.to_string()

        cond do
          # catches integers OR floats OR booleans
          String.match?(value, ~r/^[0-9]+$/) or
            String.match?(value, ~r/^[-+]?[0-9]*(\.[0-9]+)$/) or
              String.match?(value, ~r/^(?i)(true|false)$/) ->
            r = "\"" <> key <> "\":" <> value <> ","
            xml_to_json(tl, [r | result])

          # catches strings / any other data type
          true ->
            r = "\"" <> key <> "\": \"" <> value <> "\","
            xml_to_json(tl, [r | result])
        end

      # < ... /> # NULL
      String.match?(hd, ~r/<([^;]*)\/>/) ->
        str = String.replace(hd, ["<", "/>"], "")
        r = "\"" <> str <> "\": null,"
        xml_to_json(tl, [r | result])

      # </ ... > # }
      String.match?(hd, ~r/<\/([^;]*)>/) ->
        xml_to_json(tl, ["}" | result])

      # < ... > # KEY
      String.match?(hd, ~r/<([^;]*)>/) ->
        str = String.replace(hd, ["<", ">"], "")
        r = "\"" <> str <> "\": {"
        xml_to_json(tl, [r | result])
    end
  end
end
